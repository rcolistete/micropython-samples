# Firmwares for MicroPython on [Pyboard v1.1](https://store.micropython.org/product/PYBv1.1)/[Lite v1.0](https://store.micropython.org/product/PYBLITEv1.0)/[D SF2](https://store.micropython.org/product/PYBD-SF2-W4F2)/[D SF3](https://store.micropython.org/product/PYBD-SF3-W4F2)/[D SF6](https://store.micropython.org/product/PYBD-SF6-W4F2)

Look at the [MicroPython Download site](http://micropython.org/download) for official firmwares for all types of Pyboard's. For Pyboard D, the ["MicroPython Downloads / Firmware for Pyboard D-series" site](https://micropython.org/download/pybd/) has only firmwares with single precision (FP32) on Pyboard D SF2 and SF3, and with double precision (FP64) on Pyboard D SF6 because its STM32F767VIT microcontroller has native double precision floating point hardware.

All MicroPython firmwares here were compiled from the [MicroPython source code](https://github.com/micropython/micropython), are not official and don't have any warranty, but all steps to build are open and can be reproduced.


## Firmware features

The MicroPython firmwares here are named in the form :  
```<Pyboard name>_<optional 'ulab_'><'sp_' or 'dp_'><optional 'thread_' or 'network_'><version>_<date>.dfu```  
where :

- 'ulab' means the [ulab native (in C) module](https://github.com/v923z/micropython-ulab), a numpy-like array manipulation library, is included in the firmware;
- 'sp' means single precision (FP32), while 'dp' is double precision (FP64) for float point numbers;
- 'thread' means firmwares containing the ['_thread' module](https://docs.micropython.org/en/latest/library/_thread.html) and allowing multithreading;
- 'network' means Pyboard v1.1/Lite v1.0 firmware with drivers for [CC3000 WiFi modules](https://docs.micropython.org/en/latest/library/network.CC3K.html) and [WIZnet5x00 Ethernet modules](https://docs.micropython.org/en/latest/library/network.WIZNET5K.html).

For example :  
```pybd-sf6_ulab_dp_thread_v1.12-658-gdf37e3fab_2020-07-26.dfu```  
means it is a v1.12-658-gdf37e3fab firmware, from July 26 2020, with ulab module included, double precision float point numbers and threads enabled, for Pyboard D SF6.

### Single x Double precision

See some Pyboard D benchmark calculation using single x double precision in the [MicroPython forum topic 'Pyboard D double precision'](https://forum.micropython.org/viewtopic.php?f=20&t=6481&p=40723#p40723)


## Flashing firmware on Pyboard

For flashing firmware on Pyboard, the dfu mode should be enabled, depending on the Pyboard type :  
- Pyboard v1.1/Lite v1.0, see the ["MicroPython Downloads / Firmware for Pyboard v1" site](https://micropython.org/download/pybv1/), which points to [Windows instructions](http://micropython.org/resources/Micro-Python-Windows-setup.pdf) and [Pyboard Firmware Update Wiki for Linux/Mac OS](https://github.com/micropython/micropython/wiki/Pyboard-Firmware-Update);
- ###### Pyboard D, see the ["MicroPython Downloads / Firmware for Pyboard D-series" site](https://micropython.org/download/pybd/).