MicroPython com M5Stack, M5StickC :
===================================

M5Stack/M5StickC/Atom Lite/Matrix (UIFlow) MicroPython firmware era baseado em [MicroPython_
ESP32_psRAM_LoBo](https://github.com/loboris/MicroPython_ESP32_psRAM_LoBo/wiki) até UIFlow < 1.3.2, depois passou 
a usar MicroPython oficial (com modificações/personalizações pela empresa M5Stack).


UIFlow online, na China :
-------------------------

Permite escrever em Blockly e converter para MicroPython :  
http://flow.m5stack.com/  
Útil para programar em Blockly e converter para MicroPython, mesmo sem conectar a placa alguma.  
Vantagem de ter a versão mais atual, pois o UIFlow IDE idealmente precisa ser da mesma versão do firmware.

Editores offline para usar MicroPython em M5Stack e M5StickC :
------------------------------------------------------

- VSCode (Visual Studio Code) tem extensão "vscode-m5stack-mpy";   (não conecta no Dell XPS15 com Manjaro)
- UIFlow Desktop IDE offline (https://m5stack.com/pages/download) para Linux, Mac OS, Windows;

UIFlow (Blockly & MicroPython)  :
---------------------------------

Documentação :  
https://m5stack.com/pages/uiflow   
https://docs.m5stack.com/#/en/uiflow/uiflow_home_page  
https://docs.m5stack.com/#/en/quick_start/m5core/m5stack_core_get_started_MicroPython  
https://m5stack.github.io/UIFlow_doc/en/  
PDF da comunidade, "UIFlow Reference V1.4.4 - Adam Bryant", 268 páginas :  
https://github.com/Ajb2k3/UIFlowHandbook

Código-fonte, exemplos, bibliotecas, drivers, firmwares, etc, porém atualizado pela última vez em meados de 2019 :  
https://github.com/m5stack/UIFlow-Code  
Exemplos diversos, porém atualizado pela última vez em Agosto de 2019 :  
https://github.com/m5stack/UIFlow-Code/wiki

Firmwares :
-----------
Mais fácil é fazer download e instalar via UIFlow-Desktop-IDE, vide seção abaixo.  
http://firmware-repo-list.m5stack.com/firmware/UIFlow-v1.4.5.zip  (link oficial, mude a versão no link. Tem versão para Atom Lite/Matrix >= v1.4.5)  
https://github.com/EeeeBin/UIFlow-Firmware  (tem versão para M5StickC, porém atualizado pela última vez em meados de 2019)  
https://github.com/EeeeBin/UIFlow-Firmware/tree/beta (idem acima, versões mais novas, como v1.4.3)  
https://github.com/tuupola/micropython-m5stack   (sobre o firmware do Loboris)  
https://github.com/m5stack/UIFlow-Code/tree/master/firmware  
https://github.com/m5stack/M5Stack_MicroPython/tree/master/MicroPython_BUILD/firmware  (não tem 'm5stack', etc, precisa compilar para ficar mais completo)  
https://github.com/curdeveryday/test-firmware

M5Cloud (uso online), antecessor do UiFlow, meio antigo (1-2 anos) :
--------------------------------------------------------------------
Com documentação simples :  
https://github.com/m5stack/M5Cloud  
Exemplos :  
https://github.com/m5stack/M5Cloud/tree/master/examples  
Firmwares online (de 2 anos atrás) :  
https://github.com/m5stack/M5Cloud/tree/master/firmwares  
e offline (de 2 anos atrás) :  
https://github.com/m5stack/M5Cloud/tree/master/firmwares/OFF-LINE  

M5Stack Faces (com teclado Qwerty, etc), mostrando REPL na tela :
---------------------------------------------
http://forum.m5stack.com/topic/420/solved-where-to-get-basic-offline-micropython-firmware-with-lcd-module-support/21  
http://forum.m5stack.com/topic/1513/faces-micropython-repl/5


M5Stack/M5StickC com UIFlow-Desktop-IDE_Linux :
===============================================
Download de "UIFlow-Desktop-IDE" versão Linux em :
https://m5stack.com/pages/download
Guia de instalação e configuração :
https://docs.m5stack.com/#/en/uiflow/UIFlow_Desktop_IDE
Descompactar e rodar "bin/uiflow-desktop-ide". Versão 1.3.2, 1.0.6 instalada, download de 01/2020.
Versão para Windows é mais recente, v1.4.5 1.0.8 no início de 03/2020. 
UIFlow-Desktop-IDE tem :
* arquivos "UIFlow-Desktop-IDE", "bin/uiflow-desktop-ide" e "flash.sh" dos firmwares com final de linha padrão Windows, converter para padrão Linux (alguns 
editores de texto fazem isso) para o script rodar;
* janela de entrada, para escolher porta USB, seleção de device "Core" (M5Stack) ou "Stick-C";
* menu no canto superior esquero "Firmwareburner", abre janela "M5Burner", com erase/flash firmware (UIFlow_v1.45, M5Faces-Kit-v1.0.0, etc).
* firmware "UIFlow_v1.45 - Base Micropython v1.11", escolher hardware via "Series : Stack-EN" (ou StickC ou ATOM-Lite ou ATOM-Matrix);
* se via interface gráfica der error em "Erase"/"Burn", então se pode usar terminal e gravar firmware executando "flash.sh" (antes "chmod +x flash.sh") em :
"assets/firmwares/UIFlow-v1.4.5/UIFlow-Firmware-1.4.5/firmware_en/"
"assets/firmwares/UIFlow-v1.4.5/UIFlow-Firmware-1.4.5/firmware_StickC/"
"assets/firmwares/UIFlow-v1.4.5/UIFlow-Firmware-1.4.5/firmware_ATOM/"

Arquivos pré-instalados na memória flash interna eu copiei para meu Dropbox :
/Dropbox/Microcontroladores/MicroPython/0 Softwares & Libraries/0 MicroPython firmware/0 ESP32/0 M5Stack/0 UIFlow MicroPython EeeeBin firmware/UIFlow-Firmware-1.4.3/firmware_StickC/flash

Sem 'boot.py' o M5Stack/StickC trava, então não pode apagar, mas somente substituir o 'boot.py'.