import sys 
sys.path.append('flowlib/lib')
import m5base
from m5stack import lcd
import gc
import os
import time

__VERSION__ = m5base.get_version()

lcd.clear(lcd.BLACK)
lcd.image(lcd.CENTER, lcd.CENTER, "image_app/M5GO.JPG")
time.sleep_ms(1000)
lcd.clear(lcd.BLACK)
lcd.image(lcd.CENTER, 17, 'img/uiflow_logo_80x80.bmp')
lcd.setColor(0xCCCCCC, 0)
lcd.print(__VERSION__, lcd.CENTER, 10)
lcd.font(lcd.FONT_DefaultSmall)
lcd.print("MicroPython", lcd.CENTER, 100)
lcd.print('v' + os.uname().release, lcd.CENTER, 111)
lcd.print(os.uname().version[-10:], lcd.CENTER, 122)
gc.collect()
lcd.print("Free RAM", lcd.CENTER, 139)
lcd.print('{:.2f}'.format(gc.mem_free()/1024) + ' kB', lcd.CENTER, 150)
time.sleep_ms(1000)
