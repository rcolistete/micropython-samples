# MicroPython on ESP32

Tutorials, examples, tests (benchmarks), firmwares with many options (double precision, ulab module, etc), etc, about MicroPython on ESP32 boards.

TO DO :
- Using LoLin32;
- Using LoLin32 Pro;
- Using TinyPICO;
- Using shells, editors and IDE's with ESP32 board;
- benchmarks about :
    - speed of GPIO output toggle;
    - reading ADC;
    - writing to DAC;
    - etc;
- tests of power usage using USB or battery;
- tests of time to wake up from lightsleep and deepsleep;
- firmwares ESP32 with :
    - frozen modules;
    - etc.