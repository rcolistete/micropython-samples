# Using Expansion Board

##### Authors : [Ricardo Restos Loss Storoz](https://gitlab.com/ricardo.retsos), [Roberto Colistete Jr.](https://gitlab.com/rcolistete)

##### Last updated : August 11th 2020.

Pycom's Expansion Board is compatible with all xxPy cards (WiPy 1, WiPy 2, WiPy 3, LoPy v1, SiPy, LoPy4, FiPy and GPy) and allows multiple connections, making development much easier. All Expansion Board v2.x/v3.x cards have :

* female connectors exposing all 28 xxPy pins (LoPy4, etc);
* microUSB port for power supply and serial USB communication;
* microSD reader/writer;
* JST PH 2.0 connector and [BQ24040 Li-Ion/LiPo battery charger circuit](https://www.ti.com/product/BQ24040) (charges with 100 or 450 mA), with battery voltage reversal protection via [TPS2115A integrated circuit](https://www.ti.com/product/TPS2115A) if there is power via USB;
* LED 'USB' indicating USB connection;
* LED 'CHG' indicating ((battery ) 'charging';
* User configurable 'LED';
* User button, 'user push button';
* Voltage divider, with 2 resistors, connected to the VBATT/G3/P16 pin to read the battery voltage via ADC without breaking the 3.3V limit;
* 7 mini-jumpers for configuration, see [section below](## 5. Configuration of mini-jumpers).

**ATTENTION :**
- **xxPy must be connected to the Expansion Board such that the RGB LED is close to the microUSB connector, as well as all the xxPy pins should be inserted;**
- **check (with multimeter) the battery polarity before connecting, as you can burn the Expansion Board** if the polarity is reversed and has USB power;
- **when using FiPy or GPy with Expansion Board, remove the RTS and CTS mini-jumpers**, as they conflict with the operation of the LTE modem, see [CAT-M1](https://docs.pycom.io/tutorials/lte/cat-m1/) and [NB-IoT](https://docs.pycom.io/tutorials/lte/nb-iot/) tutorials.

Pycom Expansion Boards have evolved into several versions: v?, v2.0, v2.1, v2.1A, v3.0 and v3.1.


## 1. Documentation :


1. [Pycom's official website for Expansion Board v2.x](https://docs.pycom.io/datasheets/boards/expansion2/), which shows the meaning of the mini-jumpers;
2. [Official Pycom manual for Expansion Board v2.0](https://github.com/wipy/wipy/blob/master/docs/User_manual_exp_board.pdf), although old is highly recommended for having several details not available on the Pycom website of the official documentation, such as block diagram, description of mini-jumpers, source code examples, etc;
3. [Pycom's official website for Expansion Board v3.x](https://docs.pycom.io/datasheets/boards/expansion3/), where the pinout figure shows wrong resistors, while the PDF version is correct with 2 resistors of 1 MOhm;
4. ["Pycom Expansion Board 2.0 Overview" tutorial](https://core-electronics.com.au/tutorials/pycom-expansion-board-2-0-overview.html) by Core Electronics;
5. ["Pycom Expansion Board - Getting Started"  tutorial](https://core-electronics.com.au/tutorials/pycom-expansion-board-2-0-getting-started.html) by Core Electronics.

The description of the female connectors on the Expansion Board v2.x and v3.0 cards uses 'Gn' notation, while the Expansion Board v3.1 adopted the 'Pn' notation. See the relationship between 'Gn' and 'Pn' notations :

- ['pins.csv' in the official Pycom MicroPython source code](https://github.com/pycom/pycom-micropython-sigfox/blob/Dev/esp32/boards/WIPY/pins.csv);
- [Topic 'Mapping of G pins and P pin on Exp. Board v3' in the Pycom forum](https://forum.pycom.io/topic/3919/mapping-of-g-pins-and-p-pin-on-exp-board-v3?_=1592400643509).


## 2. Expansion Board v2.x

The Expansion Board v2.x (released in 2015) has:
- FTDI serial USB integrated circuit, seen on Linux as '/dev/ttyUSB0';
- manual bootloader mode, being necessary to put wire/jumper between G23/P2 and GND pins before connecting the USB power to update the MicroPython firmware of xxPy (LoPy4, etc.).
- ["Safe Boot" mode](https://docs.pycom.io/gettingstarted/programming/safeboot/), being necessary to place wire/jumper between G28/P12 and 3V3 pins before connecting the USB cable;
- voltage divider with 56 kOhm resistors (between GND and VBATT/G3/P16 pin) and 115 kOhm (between battery and VBATT/G3/P16 pin) to read the voltage of the Li-Ion/Li-Po battery (3.6-3.7 V nominal), usually between 3.0 V (well discharged) to 4.2 V (well charged), without breaking the 3.3 V limit of the ESP32/xxPy ADC;
- average power consumption in deep sleep mode, minimum of 156 uA with WiPy3 and battery providing 3.61-3.66 V in the JST PH 2.0 connector.

<img src="https://cdn-shop.adafruit.com/1200x900/3344-02.jpg" title="Expansion Board v2.1A" alt="Expansion Board v2.1A" width="600"/>


## 3. Expansion Board v3.x

The Expansion Board v3.0 (released in early 2018) has :
- PIC (low power consumption) microcontroller, with its own firmware, working as a serial USB circuit (seen on Linux as '/dev/ttyACM0'), bootloader manager and power saving mode (deepsleep) manager;
- automatic bootloader mode, allowing you to update MicroPython firmware of xxPy (LoPy4, etc.) without the need of jumper between G23/P2 and GND;
- "Safe Boot" button to easily enter [safe boot mode](https://docs.pycom.io/gettingstarted/programming/safeboot/), without having to connect G28/P12 and 3V3 pins;
- voltage divider with resistors of 1 MOhm (between GND and VBATT/G3/P16 pins) and 1 MOhm (between battery and VBATT/G3/P16 pin) to read the voltage of the Li-Ion/Li-Po battery (3.6-3.7 V nominal), usually between 3.0 V (well discharged) to 4.2 V (well charged), without breaking the 3.3 V limit of the ESP32/xxPy ADC;
- median power consumption in deep sleep mode, minimum of 233 uA with WiPy3 and battery supplying 3.61-3.66 V in the JST PH 2.0 connector;
- [**bug that prevents booting when using battery power**](https://forum.pycom.io/topic/3436/expansion-board-3-0-battery-power-not-enough-juice). **Solution is to connect a wire/jumper between the G15/P8 and 3V3 pins**, but if you are going to use the Expansion Board uSD unit then a 10 kOhm resistor should be placed between the G15/P8 and 3V3 pins. See notice in the official documentation, [ "To use the battery, pull P8/G15 high (connect to 3v3). If you want to use the SD card as well, use a 10k pull-up"](https://docs.pycom.io/datasheets/boards/expansion3/).

<img src="https://cdn.sparkfun.com//assets/parts/1/2/8/7/4/14675-LoPy_Expansion_Board_2.0-04.jpg" title="Expansion Board v3.0" 
alt="Expansion Board v3.0" width="600"/>

Expansion Board v3.1 (released in early 2019) changes from v3.0 :
- it has a white square with QRcode at the top next to the microSD, useful to differentiate from the Expansion Board v3.0 without looking at the bottom;
- description of the female connectors using 'Pn' notation instead of 'Gn';
- **low power consumption in deep sleep mode, minimum of 34 uA with WiPy3 and battery supplying 3.61-3.66 V in the JST PH 2.0 connector**, similar to the [33 uA promised by Pycom](https://forum.pycom.io/topic/4336/new-v3-1-expansion-board-firmware-v0-0-11);
- **(feature or bug?) higher power consumption in active mode (122-128 mW more) and light sleep (113 mW more) when using TX/RX mini-jumpers (serial USB) and battery power**;
- **fixed the bug that prevented booting when using battery power, no longer needing to connect a wire/jumper** (or 10 kOhm resistor) between the G15/P8 and 3V3 pins.

<img src="https://pycom.io/wp-content/uploads/2020/03/Website-Product-Shots-ExpB-front-WiPy.png" title="Expansion Board v3.1 com WiPy 3" 
alt="Expansion Board v3.1 com WiPy 3" width="700"/>


In the test source codes:
- type 'Tab' key after '.' at the end of the line to see the list of object attributes;
- outputs/results are indented with 1 more tab;
- official Pycom MicroPython v1.20.2.rc7 was used.

### 3.1. Firmware installation on Expansion Board v3.x

Expansion Board v3.x uses PIC microcontroller that has a firmware independent of the MicroPython firmware of Pycom xxPy cards (LoPy4, WiPy, FiPy, etc.).

You can't read and know which firmware version is already installed on the Expansion Board v3.x card. So it is important at least once to install the current version of the Pycom firmware for Expansion Board v3.x and put a label on the board with the firmware version.

Follow [Pycom documentation for firwmare installation on Expansion Board v3.x, Pytrack and Pysense](https://docs.pycom.io/pytrackpysense/installation/firmware/) :

1. download the firmware "Expansion Board DFU v3.0" (latest version is "expansion3_0.0.9.dfu") for Expansion Board v3.0 and the firmware "Expansion Board DFU v3.1" (latest version is "expansion31_0.0.11.dfu") for Expansion Board v3.1;

2. install 'dfu-util' on the computer. On Linux with Debian and derivatives, using terminal :  
`$ sudo apt-get install dfu-util`

3. you can install the Expansion Board v3.x card firmware without having xxPy (LoPy4, WiPy3, FiPy, etc.), by the way, this is recommended (less hardware to give problem...);

4. following to the letter, it usually works after trying a few times because of the time intervals:
	- unplug the USB cable from the Expansion Board v3.x card from the computer;
	- press the 'S1' button and hold down while connecting the USB cable between the computer and the Expansion Board v3.x;
	- wait 1 second or a little longer before releasing the 'S1' button;
	- you have approximately 7 seconds to run the dfu-util command using a terminal on the Linux computer, the tip is to leave the entire line typed, just by pressing "Enter";

5. on Expansion Board v3.0 :
```
$ sudo dfu-util -D expansion3_0.0.9.dfu
    dfu-util 0.9
    
    Copyright 2005-2009 Weston Schmidt, Harald Welte and OpenMoko Inc.
    Copyright 2010-2016 Tormod Volden and Stefan Schmidt
    This program is Free Software and has ABSOLUTELY NO WARRANTY
    Please report bugs to http://sourceforge.net/p/dfu-util/tickets/
    
    Match vendor ID from file: 04d8
    Match product ID from file: ef99
    Opening DFU capable USB device...
    ID 04d8:ef99
    Run-time device DFU version 0100
    Claiming USB DFU Runtime Interface...
    Determining device status: state = dfuIDLE, status = 0
    dfu-util: WARNING: Runtime device already in DFU state ?!?
    Claiming USB DFU Interface...
    Setting Alternate Setting #0 ...
    Determining device status: state = dfuIDLE, status = 0
    dfuIDLE, continuing
    DFU mode device DFU version 0100
    Device returned transfer size 64
    Copying data from PC to DFU device
    Download	[=========================] 100%        16384 bytes
    Download done.
    state(2) = dfuIDLE, status(0) = No error condition is present
    Done!
```

6. on Expansion Board v3.1 :
```
$ sudo dfu-util -D expansion31_0.0.11.dfu
    dfu-util 0.9
    
    Copyright 2005-2009 Weston Schmidt, Harald Welte and OpenMoko Inc.
    Copyright 2010-2016 Tormod Volden and Stefan Schmidt
    This program is Free Software and has ABSOLUTELY NO WARRANTY
    Please report bugs to http://sourceforge.net/p/dfu-util/tickets/
    
    Match vendor ID from file: 04d8
    Match product ID from file: ef99
    Opening DFU capable USB device...
    ID 04d8:ef99
    Run-time device DFU version 0100
    Claiming USB DFU Runtime Interface...
    Determining device status: state = dfuIDLE, status = 0
    dfu-util: WARNING: Runtime device already in DFU state ?!?
    Claiming USB DFU Interface...
    Setting Alternate Setting #0 ...
    Determining device status: state = dfuIDLE, status = 0
    dfuIDLE, continuing
    DFU mode device DFU version 0100
    Device returned transfer size 64
    Copying data from PC to DFU device
    Download        [=========================] 100%        16384 bytes
    Download done.
    state(2) = dfuIDLE, status(0) = No error condition is present
    Done!
```

## 4. Use in serial USB communication for REPL terminal access

### 4.1. Expansion Board v2.x

The serial USB interface is seen on Linux as '/dev/ttyUSB0'.

Example of shell tools on Linux :
- 'screen' for REPL terminal access:
```
$ screen /dev/ttyUSB0 115200
```
- [rshell](https://github.com/dhylands/rshell) :
```
$ rshell -p /dev/ttyUSB0
```

### 4.2. Expansion Board v3.x

The serial USB interface is seen on Linux as '/dev/ttyACM0'.

Example of shell tools on Linux :
- 'screen' for REPL terminal access:
```
$ screen /dev/ttyACM0 115200
```
- [rshell](https://github.com/dhylands/rshell) :
```
$ rshell -p /dev/ttyACM0
```


## 5. Configuration of mini-jumpers

**Attention: mini-jumpers are easy to get loosen up with movement.**  We recommend buying mini-jumpers to have as reserve in case you lose part of the Expansion Board mini-jumpers.

Another recommendation is to only use the mini-jumpers in necessary settings if you want to save energy (deepsleep mode), see [section below 'Energy consumption'.](#consumo-de-energia).

There are 7 mini-jumpers that by default are inserted:
- TX : 'Enable TX signal', mandatory to be inserted to have a USB serial (UART) connection, connected to the G0/P2 pin;
- RTS : 'Enable RTS signal', mandatory to be inserted to have flow control of the USB serial connection (UART), which is optional, connected to the G6/P19 pin;
- RX : 'Enable RX signal', mandatory to be inserted to have a serial USB connection, connected to the G1/P1 pin;
- CTS : 'Enable CTS signal', mandatory to be inserted to have flow control of the USB serial connection (UART), which is optional, connected to the G7/P20 pin;
- BAT : 'Enable VBATT signal', mandatory to be inserted to have the battery voltage on the VBATT/G3/P16 pin to be [read by ESP32/xxPy ADC](#10-leitura-da-tensão-da-bateria-via-divisor-de-tensão-e-pino-vbatt);
- LED: 'Enable LED signal', mandatory to be inserted to control the "User LED" via G16/P9 pin, [see example below](#uso-do-user-led);
- CHG : 'Enable CHG signal', inserted to charge battery with 450 mA, otherwise it charges with 100 mA. Be careful that the [BQ24040 charger circuit](https://www.ti.com/product/BQ24040) of Li-Ion/LiPo battery has a [safety limit of 10 hours of charge](https://forum.pycom.io/topic/821/lopy-battery/4?_=1592254492178), so if the battery has too large capacity it may not be able to fully charge depending on this configuration.


## 6. 'User button' usage

The 'User button' is of the mini-push button type and has the name "BUTTON" on Expansion Board v2.x and "S1" on Expansion Board v3.x. It is connected to the G17/P10 pin, so the reading is via digital input pin. See the [' 'Quick Usage Example' in 'Firmware & API Reference > Pycom Modules > machine > Pin'](https://docs.pycom.io/firmwareapi/pycom/machine/pin/) :
```
from machine import Pin

p_button = Pin('P10', mode=Pin.IN, pull=Pin.PULL_UP)
#p_button = Pin('G17', mode=Pin.IN, pull=Pin.PULL_UP)               # alternative notation with 'Gn', used on Expansion Board since 2015
#p_button = Pin(Pin.exp_board.G17, mode=Pin.IN, pull=Pin.PULL_UP)   # alternative form to access the notation 'Gn' for pins
#p_button = Pin(Pin.module.P10, mode=Pin.IN, pull=Pin.PULL_UP)      # alternative form to access the notation 'Pn' for pins

p_button.value()   # 1 without pressing the button
    1
p_button.value()   # 0 with pressing the button
    0
```

## 7. 'User LED' usage

The "user LED" is controlled via the G16/P9 pin, as long as the mini-jumper 'LED' is inserted.
[The pinout diagram of the Expansion Board v2.x](https://docs.pycom.io/datasheets/boards/expansion2/) shows that the logic for turning on the LED is to put the digital output of the G16/P9 pin in a low state. See the ['Quick Usage Example' in 'Firmware & API Reference > Pycom Modules > machine > Pin'](https://docs.pycom.io/firmwareapi/pycom/machine/pin/) :

```
from machine import Pin

p_led = Pin('P9', mode=Pin.OUT)
#p_led = Pin('G16', mode=Pin.OUT)                # alternative notation, citing 'Gn' naming used on Expansion Board since 2015
#p_led = Pin(Pin.exp_board.G16, mode=Pin.OUT)   # alternative form to access the notation 'Gn' for pins
#p_led = Pin(Pin.module.P9, mode=Pin.OUT)       # alternative form to access the notation 'Pn' for pins
p_led.value()   # 'user LED' is turned on when configuring the G16/P9 pin as digital output, as its default value is 0 (low)
    0
p_led.toggle()  # toggle the 'user LED', in this case from on state to off state
p_led.value()   # 'user LED' is in off state as the G16/P9 pin has value 1 (high)
    1
p_led.value(0)  # 'user LED' turned on with G16/P9 pin having value 0 (low)
p_led.value(1)  # turn off the 'user LED' by using value 1 (high)
p_led(0)        # another alternative form for G16/P9 pin having valor 0 (low), turning on the 'user LED'
p_led(1)        # turn off the 'user LED' by using value 1 (high)
```

## 8. Use of 'User button' to trigger 'User LED'

Modification of the example of ['machine.pin.callback()'](https://docs.pycom.io/firmwareapi/pycom/machine/pin/), using interrup triggered when the button is pressed (pin G17/P10 changes status from 1 to 0). Each time the button is pressed the state (on/off) of the LED changes :
```
from machine import Pin

p_led = Pin('P9', mode=Pin.OUT)
p_led.value(1)
p_button = Pin('P10', mode=Pin.IN, pull=Pin.PULL_UP)
p_button.callback(Pin.IRQ_FALLING, lambda x: p_led.toggle())
```

## 9. Use of microSD

About using the microSD (uSD) card slot of the Expansion Board card, uSD cards must have a FAT16 or FAT32 file system, with a capacity of up to 32 GB, see [Pycom documentation about the submodule 'machine.SD'](https://docs.pycom.io/firmwareapi/pycom/machine/sd/).

The internal flash memory of xxPy is seen as a '/flash' partition, while the uSD card as a '/sd' partition after the 'sd' object being instantiated and the partition mounted via 'os.mount()'. The usual (Micro)Python commands to handle files are applicable, such as open/write/close/etc and those of the ['os' module](https://docs.pycom.io/firmwareapi/micropython/uos/). But the functions 'os.ilistdir()' and 'os.statvfs()' only appear in the [official MicroPython documentation](https://docs.micropython.org/en/latest/library/uos.html#uos.statvfs).

Interactive code :
```
import machine
import os

sd = machine.SD()
sd.
    __class__       deinit          init            ioctl
    readblocks      writeblocks
os.
    __class__       __name__        remove          chdir
    dupterm         fsformat        getcwd          getfree
    ilistdir        listdir         mkdir           mkfat
    mount           rename          rmdir           stat
    statvfs         sync            umount          uname
    unlink          urandom
os.mount(sd, '/sd')     # mount the partition '/sd'
os.listdir('/sd')       # uSD with file 'test.csv'
    ['test.csv']
os.listdir()            # by default it shows the internal flash memory, '/flash'
    ['boot.py', 'cert', 'lib', 'main.py', 'sys']    
s = os.statvfs('/sd')   # 1st element is the block size in bytes, 3rd is the number of blocks in the partition, 4th is the number of free blocks
s
    (4096, 4096, 1936437, 1936019, 1936019, 0, 0, 0, 0, 128)
s[0]*s[2]/1024          # size of '/sd' partition in kB of a 8 GB uSD card
    7745748.0
s[0]*s[3]/1024          # free space in '/sd' partition in kB
    7744076.0   
os.getfree('/sd')       # simpler command to show the free space in '/sd' partition in kB
    7744076
os.getcwd()             # the default directory still points to internal flash memory, '/flash', even when mounting a uSD card
    '/flash'
os.chdir('/sd')         # changing the the default directory to the '/sd' partition
os.getcwd()
    '/sd'
f = open('/sd/test.csv', 'a')   # file open for writing in 'append' mode (append to the file if it already exists, or else create the file)
f.write('uSD with {} kB free\n'.format(os.getfree('/sd')))
f.close()               # close file
os.sync()               # syncronize writing file system buffers
os.umount('/sd')        # umount the '/sd' partition
```

Robust code for use in MicroPython scripts via the use of 'try/except' for error management:
```
import machine
import os

try:
    sd = machine.SD()
    os.mount(sd, '/sd')
    flag_uSD = True
except OSError as e:
    print('Error {} mounting uSD card'.format(e))
    flag_uSD = False

if flag_uSD:
    try:
        datalog_filename = '/sd/log.csv'
        with open(datalog_filename, 'a') as logfile:
            logfile.write('Pycom MicroPython v{}\n'.format(os.uname().release))
        os.sync()
    except OSError as e:
        print("Error {} writing to file '{}'".format(e, datalog_filename))

if flag_uSD:
    try:
        os.umount('/sd')
    except OSError as e:
        print('Error {} umounting uSD card'.format(e))
```


## 10. Battery voltage reading via voltage divider and VBATT pin

The Expansion Board has a voltage divider, with 2 resistors, connected to the VBATT/G3/P16 pin to read the voltage of the Li-Ion/Li-Po battery, which can reach 4.2 V, without breaking the 3.3V limit of the ESP32/xxPy ADC. The resistor values are different for Expansion Board versions :
- v2.x, 56 kOhm (between GND and G3/P16 pin) and 115 kOhm (between battery and G3/P16 pin), that is, the battery voltage is reduced to (56/171) = 32.75%, e.g., 4.2 V becomes 1.375 V;
- v3.x, 1 MOhm (between GND and G3/P16 pin) and 1 MOhm (between battery and G3/P16 pin), that is, the battery voltage is reduced to (1/2) = 50%, e.g., 4.2 V becomes 2.1 V.

Remember that the ESP32 ADC has several problems: offset (60-130 mV), non-linearity and high noise (so the effective resolution is about 4-5 bits lower than the nominal one), see [comparison with ADS1115](https://github.com/bboser/IoT49/blob/master/doc/analog_io.md).

Interactive code optimized for Expansion Board v2.x, using a scale of 6 dB (0.0-2.195 V) of the ESP32/xxPy ADC to better take advantage of the 12-bit resolution, with the voltage divider (115 kOhm + 56 kOhm) the battery voltage reading scale is 0.0-6.702 V :
```
import machine

adc = machine.ADC()
adc.
    __class__       ATTN_0DB        ATTN_11DB       ATTN_2_5DB
    ATTN_6DB        channel         deinit          init
    vref            vref_to_pin
adcP16 = adc.channel(pin='P16', attn=machine.ADC.ATTN_6DB)
adcP16.
    __class__       value           deinit          init
    value_to_voltage                voltage
adcP16.voltage()   # 1.390 V in G3/P16 pin, under USB power supply
    1390
(171*_)//56        # 4.244 V in battery connector, under USB power supply
    4244
adcP16.voltage()   # 1.179 V in G3/P16 pin, under battery supply with nominal 3.7 V
    1179
(171*_)//56        # 3.600 V in battery connector, under battery supply with nominal 3.7 V
    3600
```

Interactive code optimized for Expansion Board v3.x, using a scale of 11 dB (0.0-3.903 V, but limited to 3.3 V) of the ESP32/xxPy ADC, with the voltage divider (1 MOhm + 1 MOhm) the battery voltage reading scale is 0.0-6.6 V :
```
import machine

adc = machine.ADC()
adc.
    __class__       ATTN_0DB        ATTN_11DB       ATTN_2_5DB
    ATTN_6DB        channel         deinit          init
    vref            vref_to_pin
adcP16 = adc.channel(pin='P16', attn=machine.ADC.ATTN_11DB)
adcP16.
    __class__       value           deinit          init
    value_to_voltage                voltage
adcP16.voltage()   # 2.413 V in G3/P16 pin, under USB power supply
    2413
(2*_)//1            # 4.826 V  in battery connector (???), under USB power supply
    4826
adcP16.voltage()    # 1.810 V in G3/P16 pin, under battery supply with nominal 3.7 V
    1810
(2*_)//1            # 3.620 V in battery connector (???) under battery supply with nominal 3.7 V
    3620
```

Code for use in MicroPython scripts, with calculation of mean and standard deviation of ADC measurements via 
[MicroPython 'statistics' module'](https://github.com/rcolistete/MicroPython_Statistics). Take/put comments to choose between Expansion Board v2.x and v3.x :

```
import machine
import statistics
    
NUM_ADC_READINGS = const(50)

adc = machine.ADC()
adc.vref(1126)  # put here ESP32 VRef measured in mV with a voltmeter
# adcP16 = adc.channel(pin='P16', attn=machine.ADC.ATTN_6DB) # For Expansion Board v2.x
adcP16 = adc.channel(pin='P16', attn=machine.ADC.ATTN_11DB)  # For Expansion Board v3.x
samples_voltage = [0]*NUM_ADC_READINGS
for i in range(NUM_ADC_READINGS):
#    samples_voltage[i] = (171*adcP16.voltage())//56   # Expansion Board v2.x has voltage divider (115K + 56K) -> voltage scale = [0, 6702] mV
    samples_voltage[i] = (2*adcP16.voltage())//1       # Expansion Board v3.x has voltage divider (1M + 1M) -> voltage scale = [0, 6600] mV
batt_mV = round(statistics.mean(samples_voltage))
batt_mV_error = round(statistics.stdev(samples_voltage))
```


## 11. Energy consumption

### 11.1. USB cable power with [UM25C](https://www.aliexpress.com/item/32855845265.html)

Voltage Vusb = 5.06-5.08 V.

WiPy 3 (with PyBytes off, WiFi off, RBG LED with heartbeat off) + Expansion Board v3.0 (without uSD, with all mini-jumpers inserted) :
* i = 45.0-45.2 mA with 'User LED' (red) off;
* i = 48.3-48.6 mA with **'User LED' (red) turned on, spends 3.1-3.6 mA more;**

#### 11.1.1. Expansion Board v2.x/v3.x with WiPy 3 in active mode, USB power supply

WiPy 3 with PyBytes off, WiFi off, LED RBG with heartbeat off.

For comparison, **WiPy 3 without Expansion Board, with battery (Vbat = 3.61-3.66 V) powering directly on the VIN and GND pins, consumes 34.5-34.7 mA in active mode, i.e., Pbat = 121-123 mW**.

Configurations with and without 32GB Samsung Evo UHS-I uSD were tested.

| Expansion Board | i (ma) with all mini-jumpers | i (ma) with onlyTX/RX mini-jumpers | I (ma) without mini-jumpers |
| :-------------- | :--------------------------: | :--------------------------------: | :-------------------------: |
| v2.1A           |          38.8-39.6           |             38.3-39.0              |          38.2-39.0          |
| v3.0            |          43.5-44.2           |             42.8-43.7              |          42.9-43.7          |
| v3.1            |          43.3-44.0           |             42.8-43.5              |          42.7-43.4          |

Note that in active mode with USB power supply :
- **Expansion Board v2.1A consumes Pusb = 193-201 mW (61% more than not being used), v3.x consumes Pusb = 216-224 W (80% more)**;
- **Expansion Board v2.1A consumes around 11% less than Expansion Board v3.x**;
- with all mini-jumpers (i.e., with LED mini-jumper), consumption increases 0.5-0.6 mA compared to not having mini-jumpers;
- consumption is approximately the same with and without TX/RX mini-jumpers.

#### 11.1.2. Expansion Board v2.x/v3.x with WiPy 3 in [light sleep mode](https://docs.pycom.io/firmwareapi/pycom/machine/), USB power supply

WiPy 3 with PyBytes off, WiFi off, RBG LED with heartbeat off, no uSD.

For comparison, **WiPy 3 without Expansion Board, with battery (Vbat = 4.15-4.16 V) powering directly on the VIN and GND pins, consumes 1.94-2.02 mA in light sleep mode, i.e., Pbat = 7.80-8.18 mW.**.

| Expansion Board | I (ma) with only TX/RX mini-jumpers |
| :-------------- | :---------------------------------: |
| v2.1A           |              13.9-14.3              |
| v3.0            |              17.1-17.4              |
| v3.1            |              17.1-17.5              |

Note that in light sleep mode with USB power supply :
- **Expansion Board v2.1A consumes Pusb = 70.3-72.6 mW (8.9x of not being used), v3.x consumes Pusb = 86.5-88.9 mW (11x)**;
- **Expansion Board v2.1A consumes around 18% less than Expansion Board v3.x**;
- About mini-jumpers and uSD, see the reviews above and below.


#### 11.1.3. Expansion Board v2.x/v3.x with WiPy 3 in [deep sleep mode](https://docs.pycom.io/firmwareapi/pycom/machine/), USB power supply

For comparison, **WiPy 3 without Expansion Board, with battery (Vbat = 3.61-3.66 V) feeding directly on the VIN and GND pins, consumes 20-21 uA in deep sleep mode, i.e., Pbat = 0.0722-0.0799 mW**.

| Expansion Board |  xxPy  |          uSD           | i (ma) with all mini-jumpers | I (ma) with only TX/RX mini-jumpers |
| :-------------- | :----: | :--------------------: | :--------------------------: | :---------------------------------: |
| v2.1A           |   -    |           -            |          12.5-12.8           |              12.5-12.8              |
| v2.1A           | WiPy 3 |           -            |          12.7-13.0           |              12.5-12.9              |
| v2.1A           | WiPy 3 | 32GB Samsung Evo UHS-I |          13.1-13.4           |              12.9-13.3              |
| v3.0            |   -    |           -            |          15.3-15.6           |              15.3-15.6              |
| v3.0            | WiPy 3 |           -            |          15.7-16.0           |              15.3-15.7              |
| v3.0            | WiPy 3 | 32GB Samsung Evo UHS-I |          16.1-16.5           |              15.8-16.2              |
| v3.1            |   -    |           -            |          15.2-15.6           |              15.2-15.6              |
| v3.1            | WiPy 3 |           -            |          15.7-16.0           |              15.5-15.8              |
| v3.1            | WiPy 3 | 32GB Samsung Evo UHS-I |          16.1-16.5           |              15.9-16.2              |

Note that in deep sleep mode with USB power supply :
- without xxPy, Expansion Board's v2.1A/v3.x alone consume regardless of the configuration of mini-jumpers and uSD card inserted or not;
- **Expansion Board v2.1A consumes around 19% less than Expansion Board v3.x**;
- consumption is the same with and without TX/RX mini-jumpers;
- with all mini-jumpers (i.e., with LED mini-jumper), consumption increases 0.1-0.3 mA with respect not having mini-jumpers;
- the uSD card, even without being used or mounted, consumes energy, just be inserted. **The mere insertion of Samsung Evo UHS-I 32GB uSD card spends over 0.4-0.5 mA**, such additional consumption depends on the model of the uSD card (brand, capacity, etc.), just measuring to know.

So **to have a little lower consumption in deep sleep mode with USB power supply, use Expansion Board** :
- **v2.1**;
- **without LED mini-jumper**;
- **no uSD**, but if you need then test different models and measure the energy consumption to choose which uSD spends the least, as it can vary 10x.

**To have low consumption (< 1 mA) in deep sleep mode with Expansion Board, USB power should be avoided and Li-Ion/Li-Po battery power adopted, see [subsection 11.2.3](#1123-expansion-board-v2xv3x-com-wipy-3-em-modo-deep-sleep-alimenta%C3%A7%C3%A3o-por-bateria) below.**


### 11.2. Power via battery connector, measuring with multimeter

With 3.3 V in the battery connector the xxPy (WiPy 3, LoPy4, etc.) does not turn on if it is on the Expansion Board.

#### 11.2.1. Expansion Board v2.x/v3.x with WiPy 3 in active mode, battery power

WiPy 3 with PyBytes off, WiFi off, LED RBG with heartbeat off. Battery voltage, Vbat = 3.61-3.66 V.

For comparison, **WiPy 3 without Expansion Board, with battery (Vbat = 3.61-3.66 V) powering directly on the VIN and GND pins, consumes 34.5-34.7 mA in active mode, i.e., Pbat = 121-123 mW**.

Configurations with and without uSD 8GB Sandisk UHS-I, 16GB Toshiba M203 UHS-I and 32GB Samsung Evo UHS-I were tested.

| Expansion Board | i (ma) with all mini-jumpers | I (ma) with only TX/RX mini-jumpers | I (ma) without mini-jumpers |
| :-------------- | :--------------------------: | :---------------------------------: | :-------------------------: |
| v2.1A           |          35.0-35.7           |              34.6-35.2              |          34.6-35.2          |
| v3.0            |            39-42             |                39-42                |          34.9-35.3          |
| v3.1            |          69.6-70.2           |              68.8-69.4              |          34.5-35.0          |

Note that in active mode with battery power :
- **without mini-jumpers, the 3 Expansion Board's consume the same, Pusb = 125-131 mW (5% more than not being used)**;
- **Expansion Board v2.1A consumes the same without and with TX/RX mini-jumpers**, while **Expansion Board v3.0 has an increase of 4-7 mA (14-26 mW)** and **Expansion Board v3.1 consumes another 33.8-34.9 mA (122-128 mW)**.

So **to have low consumption in active mode, use Expansion Board** :
- **without mini-jumpers (i.e., no LED and TX/RX)**, if possible;
- **v2.1** if you need to use TX/RX mini-jumpers (serial USB mode).

#### 11.2.2. Expansion Board v2.x/v3.x with WiPy 3 in [light sleep mode](https://docs.pycom.io/firmwareapi/pycom/machine/),  battery power

WiPy 3 with PyBytes off, WiFi off, RBG LED with heartbeat off, no uSD.

For comparison, **WiPy 3 without Expansion Board, with battery (Vbat = 4.15-4.16 V) powering directly on the VIN and GND pins, consumes 1.94-2.02 mA in light sleep mode, i.e., Pbat = 7.80-8.18 mW**.

As the battery voltage ranged from 3.82-4.15 V, the power P = V*i was calculated.

| Expansion Board | P (mW) with all mini-jumpers | P (mW) with only TX/RX mini-jumpers | P (mW) without mini-jumpers |
| :-------------- | :--------------------------: | :---------------------------------: | :-------------------------: |
| v2.1A           |           8.1-8.4            |               7.9-8.2               |           7.9-8.2           |
| v3.0            |          21.1-28.2           |              21.1-28.2              |           8.0-8.2           |
| v3.1            |             120              |                 120                 |           7.2-7.3           |

Note that in light sleep mode with battery power :
- Expansion Board v2.1/v3.x without mini-jumpers, consumption is between 7-8 mW (1.74-1.99 mA @ 4.14 V);
- with TX/RX mini-jumpers, the power consumed with Expansion Board v3.0 increases to 21-28 mW (5.1-6.8 mA @ 4.13-4.14 V) and with Expansion Board v3.1 to 120 mW (29.9-30.0 mA @ 4.01 V);
- about using uSD, see the reviews below.

So **to have low consumption in light sleep mode, use Expansion Board** :
- **without mini-jumpers (i.e., no LED and TX/RX)**, if possible;
- **v2.1** if you need to use TX/RX mini-jumpers (serial USB mode).

#### 11.2.3. Expansion Board v2.x/v3.x with WiPy 3 in [deep sleep mode](https://docs.pycom.io/firmwareapi/pycom/machine/), battery power

Battery voltage, Vbat = 3.61-3.66 V.

For comparison, **WiPy 3 without Expansion Board, with battery (Vbat = 3.61-3.66 V) feeding directly on the VIN and GND pins, consumes 20-21 uA in deep sleep mode, i.e., Pbat = 0.0722-0.0799 mW.**.

| Expansion Board |  xxPy  |           uSD           |  i (ua) with all mini-jumpers | i (ua) only with TX/RX mini-jumpers |
|:----------------|:------:|:-----------------------:|:-----------------------------:|:-----------------------------------:|
| v2.1A           |   -    |            -            |            136                |               136                   |
| v2.1A           | WiPy 3 |            -            |            226                |               156                   |
| v2.1A           | WiPy 3 |    8GB Sandisk UHS-I    |            306                |               236                   |
| v2.1A           | WiPy 3 | 16GB Toshiba M203 UHS-I |            283                |               213                   |
| v2.1A           | WiPy 3 |  32GB Samsung Evo UHS-I |            817                |               747                   |
| v3.0            |   -    |            -            |              3                |                 3                   |
| v3.0            | WiPy 3 |            -            |            302                |               233                   |
| v3.0            | WiPy 3 |    8GB Sandisk UHS-I    |            383                |               313                   |
| v3.0            | WiPy 3 | 16GB Toshiba M203 UHS-I |            359                |               290                   |
| v3.0            | WiPy 3 |  32GB Samsung Evo UHS-I |            897                |               825                   |
| v3.1            |   -    |            -            |             12                |                12                   |
| v3.1            | WiPy 3 |            -            |            102                |                34                   |
| v3.1            | WiPy 3 |    8GB Sandisk UHS-I    |            183                |               114                   |
| v3.1            | WiPy 3 | 16GB Toshiba M203 UHS-I |            160                |                90                   |
| v3.1            | WiPy 3 |  32GB Samsung Evo UHS-I |            692                |               623                   |

Note that in deep sleep mode with battery power :
- without xxPy, **Expansion Board v2.1A alone consumes 136 uA, v3.0 consumes 3 uA, v3.1 consumes 12 uA**, regardless of the configuration of mini-jumpers and uSD card inserted or not;
- with WiPy3 (in deep sleep mode), without uSD, the **Expansion Board v2.1A module consumes 156 uA (226 uA with all mini-jumpers)**, **v3.0 consumes 233 uA (302 uA), v3.1 consumes 34 uA (102 uA)**;
- **removing all mini-jumpers except TX/RX saves 68-72 uA**. The LED mini-jumper is the one that consumes more and TX/RX in deep sleep mode makes no difference;
- the uSD card, even without being used or mounted, consumes energy, just be inserted. **The mere insertion of uSD card spends over 57-595 uA**, such additional consumption depends on the model of the uSD card (brand, capacity, etc.), just measuring to know.

So **to have very low consumption in deep sleep mode, use Expansion Board** :
- **v3.1**;
- **without LED mini-jumper**;
- **without uSD**, but if you need then test different models and measure energy consumption to choose which one spends the least, as it can vary 10x.

See more comparisons in the topic 
[MicroSD power consumption on Expansion Board, PyTrack, PySense, etc](https://forum.pycom.io/topic/3136/microsd-power-consumption-on-expansion-board-pytrack-pysense-etc) 
on Pycom forum.

(TO DO) :
- In various configurations make measurements with a more stable adjustable voltage source (e.g., 3.7 V) as if it were a battery connected to the JST PH 2.0 pins;
- Use adjustable voltage source to vary the voltage on the JST PH 2.0 pins and measure the dissipated power in a certain configuration.


## 12. Voltage on Vin and Vbatt/G3/P16 pins

The measures below are to try to clarify the difference in voltage of power supply (by battery or USB), Vin and Vbatt/G3/P16.

The block diagram of the [official Pycom manual, from Expansion Board v2.0](https://github.com/wipy/wipy/blob/master/docs/User_manual_exp_board.pdf) :
- Shows that Expansion Board has a Vin pin output of the xxPy board, coming from USB power or battery and passing through different integrated circuits (which obviously generate voltage drop);
- It does not show exactly where there is output for Vbatt/G3/P16 pin, for example, if before or after the "Reverse Polarity Protection", so there is no confirmation if it is 100% of the battery voltage that arrives at the voltage divider connected to the Vbatt/G3/P16 pin of the xxPy board.

See in [section 10](#10-leitura-da-tensão-da-bateria-via-divisor-de-tensão-e-pino-vbatt) that the Vbatt/G3/P16 (V) pin has the battery voltage (near point) reduced to :
- (56/171) = 32.75% on Expansion Board v2.x;
- (1/2) = 50% on Expansion Board v3.x. But consider a tolerance of 1% of the voltage divider resistors.

| Expansion Board |  xxPy  |            Voltage             |   Vin (V)   | Vbatt/G3/P16 (V) |
| :-------------- | :----: | :----------------------------: | :---------: | :--------------: |
| v2.1A           |   -    |      Vbat = 4.130-4.131 V      | 4.130-4.131 |      1.348       |
| v2.1A           | WiPy 3 |         Vbat = 4.129 V         |    4.129    |      1.347       |
| v2.1A           |   -    | Vusb = 4.819 V without battery |    4.804    |      1.305       |
| v2.1A           | WiPy 3 | Vusb = 5.125 V without battery |    5.108    |      1.305       |
| v2.1A           |   -    | Vusb = 5.121 V without battery |    5.058    |      1.368       |
| v2.1A           | WiPy 3 | Vusb = 5.122 V without battery |    5.060    |      1.368       |
| v3.0            |   -    |         Vbat = 4.130 V         |    4.130    |      1.976       |
| v3.0            | WiPy 3 |      Vbat = 4.130-4.132 V      | 4.129-4.130 |   1.972-1.975    |
| v3.0            |   -    | Vusb = 5.125 V without battery | 5.005-5.006 |      2.395       |
| v3.0            | WiPy 3 | Vusb = 5.125 V without battery |    4.927    |   2.353-2.357    |
| v3.0            |   -    | Vusb = 5.113 V without battery |    4.863    |      2.328       |
| v3.0            | WiPy 3 | Vusb = 5.116 V without battery |    4.820    |      2.303       |
| v3.1            |   -    |         Vbat = 4.130 V         |    4.130    |      1.966       |
| v3.1            | WiPy 3 |         Vbat = 4.121 V         |    4.118    |   1.958-1.959    |
| v3.1            |   -    | Vusb = 5.125 V without battery | 4.869-4.870 |      2.317       |
| v3.1            | WiPy 3 | Vusb = 5.125 V without battery | 4.858-4.859 |   2.302-2.307    |
| v3.1            |   -    | Vusb = 5.110 V without battery |    4.625    |      2.165       |
| v3.1            | WiPy 3 | Vusb = 5.099 V without battery |    4.579    |   2.153-2.154    |

Note that :
- Voltage on the Vin pin <> voltage on the Vbatt/G3/P16 pin after dividing by the voltage divider factor, the difference can be between less than 1% and a 7%;
- When powered by battery, Vin is equal to or very little (mV) lower than the battery voltage, Vbat;
- When it has USB power, Vin is smaller (hundredths to tenths of V) than the USB voltage, the difference increases with the electric current consumed, for example when a battery is being charged;
- The voltage on the Vbatt/G3/P16 pin, after dividing by the voltage divider factor, is very close to the battery voltage in the case of the Expansion Board v2.1 (error < 1%), but with Expansion Board v3.x the value is about 5% lower;
- Via Vbatt/G3/P16 pin reading it is difficult to detect between USB power versus power via heavily charged battery, so to detect the presence of USB power it is better to read Vin (via another voltage divider and ADC on another pin).
