# Usando FiPy

##### Autor : Roberto Colistete Jr. (roberto.colistete arroba gmail.com)
##### Última atualização : 17/06/2020.

[FiPy é uma placa anunciada pela Pycom no final de 2016](https://www.kickstarter.com/projects/pycom/fipy-the-worlds-first-5-network-iot-dev-board) 
que começou a ser entregue no final de 2017, com microcontrolador ESP32, memória PSRAM de 4 MB, memória flash de 8 MB, 5 tipos 
de conexão sem fio (WiFi, Bluetooth, LoRa, Sigfox, LTE-M CAT-M1/NB-IoT), [certificação (CE, FCC, LoRaWAN e Sigfox)](https://docs.pycom.io/documents/certificates/), 
alta qualidade de construção, com baixíssimo consumo em modo deepsleep. [FiPy na Pycom Store custa EUR 54](https://pycom.io/product/fipy/).

<img src="https://pycom.io/wp-content/uploads/2018/08/fipySide.png" alt="FiPy com pinos" width="400"/>


## 1. Documentação do FiPy e MicroPython :

- [Documentação do Pycom MicroPython](https://docs.pycom.io/);
- [FiPy datasheet, diagrama de pinagem e vários links](https://docs.pycom.io/datasheets/development/fipy/);
- ['Getting Started' com FiPy](https://docs.pycom.io/gettingstarted/connection/fipy/).

**ATENÇÂO : sempre ter conectadas as antenas LoRa** (conector ao lado do LED RGB) **e LTE-M** (parte inferior) ao usar FiPy. 

<img src="https://docs.pycom.io/gitbook/assets/fipy-pinout.png" alt="Descrição da pinagem do FiPy" width="1000"/>

Especificações do FiPy, vide também [site da Expressif sobre ESP32](https://www.espressif.com/en/products/socs/esp32/overview) :
- ESP32 (ESP32D0WDQ6 rev. 1) com 2x 32 bit Xtensa 32-bit LX6 @ 160/240 MHz, suporte a número de ponto flutuante com precisão simples;
- 520 KB SRAM e 4 MB PSRAM (via QSPI);
- 448 kB ROM e 8 MB de memória flash (via QSPI);
- WiFi 802.11b/g/n 150 MBps, Bluetooth v4.2 BR/EDR and BLE;
- [transceiver Semtech SX1272](https://www.semtech.com/products/wireless-rf/lora-transceivers/sx1272) para LoRa com bandas 868 MHz e 
915 MHz e para Sigfox RCZ1-4;
- transceiver Sequans LTE-M CAT-M1/NB-IoT;
- 34 GPIO (24 pinos expostos no FiPy, mas 5 usados por LTE-M, 3 por LoRa/Sigfox, pino P2 é para LED RGB/boot, pino P12 é para antena 
WiFi/modos de boot);
- 18 canais de ADC de 12 bits, 2 canais de DAC de bits, 16 portas PWM;
- 2x I2C (400 kHz master/slave), 4x SPI (up to 80 MHz master/slave), 2x I2S;
- 4x timers (64 bits com prescalers de 16-bits), 13 canais DMA, 3x UARTs;
- RTC com cristal de 32,768 kHz e 16 kB SRAM (8 kB slow SRAM, 8 kB fast SRAM);
- antena interna para WiFi, conectores para antenas externas para LoRa/Sigfox, LTE-M (CAT-M1 and NB-IoT) e WiFi;
- conector Nano SIM para LTE-M;
- RGB LED WS2812;
- botão de reset;
- dimensões sem pinos : 55 mm x 20 mm x 3.5 mm;
- peso sem pinos : 7 g.


Nos códigos-fonte de testes :
- digite tecla 'Tab' após '.' em final de linha para ver a lista de atributos do objeto;
- saídas/resultados estão indentados com 1 tabulação a mais.


## 2. LTE-M 

- [Documentação 'Getting Started - Cellular Device Registration'](https://docs.pycom.io/gettingstarted/registration/cellular/);
- [Tutorial e exemplos sobre LTE-M](https://docs.pycom.io/tutorials/lte/);
- [Lista de funções sobre lTE-M](https://docs.pycom.io/firmwareapi/pycom/network/lte/);
- [Documentação sobre atualização do firmware do modem LTE-ME](https://docs.pycom.io/tutorials/lte/firmware/);
- [Download dos firmwares oficiais do modem LTE](https://forum.pycom.io/topic/4020/firmware-files-for-sequans-lte-modem-now-are-secured);
- [Versões antigas firmwares oficiais do modem LTE](http://stiny.webd.pl/PYCOM/).

**ATENÇÃO : ao usar FiPy com Expansion Board, retire os mini-jumpers RTS e CTS**, pois conflitam com a operação do modem LTE, vide 
tutoriais de [CAT-M1](https://docs.pycom.io/tutorials/lte/cat-m1/) e de [NB-IoT](https://docs.pycom.io/tutorials/lte/nb-iot/).

Exemplos :

- [obter o IMEI do modem LTE](https://docs.pycom.io/tutorials/lte/imei/) :
```
from network import LTE
lte = LTE()
lte.imei()                      # p. e., '35XXXXXXXXXXXXX', onde números foram trocados por 'X' por questões de segurança
# ou :
lte.send_at_cmd('AT+CGSN=1')    # '\r\n+CGSN: "35XXXXXXXXXXXXX"\r\n\r\nOK\r\n'
```

- obter a versão do firmware do modem LTE :
```
from network import LTE
lte = LTE()
lte.send_at_cmd('ATI')   # LTE modem firmware version : '\r\nSEQUANS Communications\r\nVZM20Q\r\nUEUnknown\r\n\r\nOK\r\n'
```


## 3. Testes e exemplos com MicroPython :

- usando firmware Pycom MicroPython oficial 1.20.2.rc7 (de 04/05/2020, não foi anunciado oficialmente em 
[Pycom MicroPython releases](https://github.com/pycom/pycom-micropython-sigfox/releases)) :
```
$ screen /dev/ttyACM0 115200
Pycom MicroPython 1.20.2.rc7 [v1.11-6d01270] on 2020-05-04; FiPy with ESP32
import gc
gc.collect()
gc.mem_free()
    2560704   # 2500.687 kB de RAM livre
```

- [módulo 'os'](https://docs.pycom.io/firmwareapi/micropython/uos/) para ver versão de MicroPython, tamanho do sistema 
de arquivos da memória flash, etc :
```
import os
os.
    __class__       __name__        remove          chdir
    dupterm         fsformat        getcwd          getfree
    ilistdir        listdir         mkdir           mkfat
    mount           rename          rmdir           stat
    statvfs         sync            umount          uname
    unlink          urandom
os.uname()
    (sysname='FiPy', nodename='FiPy', release='1.20.2.rc7', version='v1.11-6d01270 on 2020-05-04', machine='FiPy with ESP32', lorawan='1.0.2', sigfox='1.0.1', pybytes='1.4.0')
os.statvfs('/flash')
    (4096, 4096, 1024, 1016, 1016, 0, 0, 0, 0, 1022)   # 4MB as file system from total of 8 MB of flash memory
```

- [módulo 'machine'](https://docs.pycom.io/firmwareapi/pycom/machine/), frequência do ESP32 (160 MHz, não dá para alterar), etc :
```
import machine
machine.
    __class__       __name__        main            ADC
    BROWN_OUT_RESET                 CAN             DAC
    DEEPSLEEP_RESET                 HARD_RESET      I2C
    PIN_WAKE        PWM             PWRON_RESET     PWRON_WAKE
    Pin             RMT             RTC             RTC_WAKE
    SD              SOFT_RESET      SPI             Timer
    Touch           UART            ULP_WAKE        WAKEUP_ALL_LOW
    WAKEUP_ANY_HIGH                 WDT             WDT_RESET
    deepsleep       disable_irq     enable_irq      flash_encrypt
    freq            idle            info            mem16
    mem32           mem8            pin_sleep_wakeup
    remaining_sleep_time            reset           reset_cause
    rng             secure_boot     sleep           temperature
    unique_id       wake_reason
machine.freq()
    160000000
machine.info()
    ---------------------------------------------
    System memory info (in bytes)
    ---------------------------------------------
    MPTask stack water mark: 8648
    ServersTask stack water mark: 3692
    LoRaTask stack water mark: 3276
    SigfoxTask stack water mark: 2940
    TimerTask stack water mark: 2212
    IdleTask stack water mark: 608
    System free heap: 373396
    ---------------------------------------------
machine.temperature()   # in F
    116
(_ - 32)/1.8            # in C
    46.66667
machine.unique_id()
    b'0\xae\xa4-\x04\xdc'
import ubinascii
ubinascii.hexlify(machine.unique_id())   # = Device ID = WiFi MAC address
    b'30aea42d04dc'
```

- [módulo 'micropython'](https://docs.pycom.io/firmwareapi/micropython/micropython/) :
```
import micropython
micropython.
    __class__       __name__        const
    alloc_emergency_exception_buf   heap_lock       heap_unlock
    kbd_intr        mem_info        opt_level       qstr_info
    stack_use
micropython.mem_info()
    stack: 736 out of 11264
    GC: total: 2561344, used: 1296, free: 2560048
    No. of 1-blocks: 38, 2-blocks: 5, max blk sz: 8, max free sz: 159779
```

- help :
```
help()
    Welcome to MicroPython!
    For online docs please visit http://docs.pycom.io

    Control commands:
    CTRL-A        -- on a blank line, enter raw REPL mode
    CTRL-B        -- on a blank line, enter normal REPL mode
    CTRL-C        -- interrupt a running program
    CTRL-E        -- on a blank line, enter paste mode
    CTRL-F        -- on a blank line, do a hard reset of the board and enter safe boot

    For further help on a specific object, type help(obj)
    For a list of available modules, type help('modules')
```

- lista de módulos mostra muitos módulos extras (pybytes, etc) da Pycom :
```
help('modules')
    _OTA              _pybytes_main     network           ucrypto
    __main__          _pybytes_protocol os                uctypes
    _boot             _pybytes_pymesh_config              pycom             uerrno
    _coap             _terminal         queue             uerrno
    _flash_control_OTA                  _thread           re                uhashlib
    _main             _urequest         select            uio
    _main_pybytes     array             socket            ujson
    _mqtt             binascii          sqnsbrz           umachine
    _mqtt_core        builtins          sqnscodec         uos
    _msg_handl        cmath             sqnscrc           uqueue
    _periodical_pin   crypto            sqnstp            ure
    _pybytes          errno             sqnsupgrade       uselect
    _pybytes_ca       framebuf          ssl               uselect
    _pybytes_config   gc                struct            usocket
    _pybytes_config_reader              hashlib           sys               ussl
    _pybytes_connection                 json              time              ustruct
    _pybytes_constants                  machine           ubinascii         utime
    _pybytes_debug    math              ubinascii         utimeq
    _pybytes_library  micropython       ucollections      uzlib
    Plus any modules on the filesystem
```

- [LED RGB](https://docs.pycom.io/firmwareapi/pycom/pycom/), vide [tutorial](https://docs.pycom.io/tutorials/all/rgbled/). 
"By default the heartbeat LED flashes in blue colour once every 4s to signal that the system is alive" :
```
import pycom
pycom.heartbeat(False)    # Desabilita a pulsação do LED RGB
pycom.heartbeat()         # Mostra o estado da configuração de pulsação do LED RGB
    False
pycom.rgbled(0x0000ff)    # Azul, brilho máximo
pycom.rgbled(0x000010)    # Azul, brilho fraco
pycom.rgbled(0x00ff00)    # Verde, brilho máximo
pycom.rgbled(0xff0000)    # Vermelho, brilho máximo
pycom.rgbled(0xffffff)    # Branco, brilho máximo
pycom.rgbled(0x000000)    # Desligado
pycom.heartbeat(True)     # Habilita a pulsação do LED RGB
pycom.heartbeat_on_boot(False)   # Desabilita a pulsação do LED RGB, efetivo só no próximo boot
```

A FAZER exemplos sobre :
- GPIO com benchmark de chaveamento de saída;
- I2C;
- ADC com benchmark;
- DAC com benchmark;
- RTC;
- NVRAM (nvs_*);
- etc.


## 4. Consumo de energia :

FiPy v1.0 + Expansion Board v3.0 só com jumpers RX/TX (ou seja, sem RTS/CTS jumpers).  
Firmware : Pycom MicroPython 1.20.2.rc7 [v1.11-6d01270] on 2020-05-04; FiPy with ESP32

<img src="https://pycom.io/wp-content/uploads/2020/03/Website-Product-Shots-ExpB-Right-Side-FiPy-1.png" alt="FiPy em placa Expansion Board" width="400"/>

### 4.1. Alimentação com cabo USB e terminal REPL aberto, com medidor USB [UM25C](https://www.aliexpress.com/item/32855845265.html).

Tensão Vusb = 5,07-5,08 V.

O default após o firmware ser gravado é ter WiFi habilitado em modo AP (Access Point) e LTE-M habilitado :
* i = 278-288 mA com PyBytes off, WiFi on (AP on/STA off), BLE on, LTE on;
* i = 276-277 mA com PyBytes off, WiFi on (AP off/STA on), BLE on, LTE on;
* i = 210-215 mA com PyBytes off, WiFi off, BLE on, LTE on;
* i =  80-112 mA com PyBytes off, WiFi off, BLE on, LTE off via 'pycom.lte_modem_en_on_boot(False), às vezes não funciona...
* i = 111-122 mA com PyBytes off, WiFi on (AP on/STA off), BLE on, LTE off via 'lte.deinit()';
* i = 109-110 mA com PyBytes off, WiFi on (AP off/STA on), BLE on, LTE off via 'lte.deinit()';
* i = 46,0-46,3 mA com PyBytes off, WiFi off, BLE on, LTE off via 'lte.deinit()';
* i = 18,3-18,7 mA em light sleep com PyBytes off, WiFi off, BLE on, LTE off via 'lte.deinit()';
* i = 16,7-17,1 mA em deep sleep com PyBytes off, WiFi off, BLE on, LTE off via 'lte.deinit()';
* LED RBG com 'pycom.heartbeat(False)' e PyBytes off, WiFi off, BLE on, LTE off via 'lte.deinit()' :
    + i = 46,0-46,3 mA com LED RBG apagado;
    + i = 84,6-85,0 mA com LED RBG branco, brilho máximo muito forte, "pycom.rgbled(0xffffff)", aumento de 38,3-39,0 mA;
    + i = 61,9-62,2 mA com LED RBG vermelho, brilho máximo muito forte, "pycom.rgbled(0xff0000)", aumento de 15,6-16,2 mA;
    + i = 59,7-60,2 mA com LED RBG verde, brilho máximo muito forte, "pycom.rgbled(0x00ff00)", aumento de 13,4-14,2 mA;
    + i = 61,5-62,0 mA com LED RBG azul, brilho máximo muito forte, "pycom.rgbled(0x0000ff)", aumento de 15,2-16,0 mA;
    + i = 64,7-65,1 mA com LED RBG branco, brilho forte, "pycom.rgbled(0x808080)", aumento de 18,4-19,1 mA;
    + i = 54,4-55,0 mA com LED RBG branco, brilho médio, "pycom.rgbled(0x404040)", aumento de 8,1-9,0 mA;
    + i = 47,0-47,4 mA com LED RBG branco, brilho fraco, "pycom.rgbled(0x101010)", aumento de 0,7-1,4 mA;
    + i = 46,4-46,9 mA com LED RBG branco, brilho muito fraco, "pycom.rgbled(0x080808)", aumento de 0,1-0,9 mA;

- [desabilitar heart beat RGB LED](https://docs.pycom.io/firmwareapi/pycom/pycom/), assim i oscila menos (somente na faixa de 0,2 mA) :
```
import pycom
pycom.heartbeat(False)
# ou :
pycom.heartbeat_on_boot(False)       # efetivo só no próximo boot
```

- [desabilitar PyBytes](https://forum.pycom.io/topic/5649/new-firmware-release-1-20-2-rc3) :
```
import pycom
pycom.pybytes_on_boot(False)         # efetivo só no próximo boot, normalmente já vem com False após gravar firmware sem habilitar PyBytes
```

- [desabilitar LTE](https://docs.pycom.io/firmwareapi/pycom/network/lte/) :
```
import network
lte = network.LTE()
lte.deinit()                         # tal comando trava o REPL por uns 10s e demora uns 25 segundos para cair o consumo, precisa rodar após cada boot...
# ou :
import pycom
pycom.lte_modem_en_on_boot(False)    # efetivo só no próximo boot, normalmente já vem com True após gravar firmware
```

- [desabilitar WiFi](https://development.pycom.io/firmwareapi/pycom/pycom/) :
```
import network
wlan = network.WLAN(mode=network.WLAN.STA)
wlan.mode()   # 1 = wlan.STA
wlan.deinit()
#
import network
wlan = network.WLAN(mode=network.WLAN.AP, ssid='FiPy')
wlan.mode()   # 2 = wlan.AP
# ou  :
import pycom
pycom.wifi_on_boot(False)            # efetivo só no próximo boot, normalmente já vem com True após gravar firmware
```

- [desabilitar BLE - Bluetooth Low Energy](https://docs.pycom.io/firmwareapi/pycom/network/bluetooth/), mas i varia fração de mA, 
difícil de medir (inclusive parece que aumenta ao rodar o 'ble.deinit()':
```
import network
ble = network.Bluetooth()
ble.deinit()
```

- [light sleep](https://development.pycom.io/firmwareapi/pycom/machine/), modo de economia de energia leve em que [a CPU é 
pausada e periféricos desligados (inclusive WiFi e Bluetooth)](https://lastminuteengineers.com/esp32-sleep-modes-power-consumption/#esp32-light-sleep), 
continuando de onde parou após terminar :
```
import machine
machine.sleep(60000)   # 60000 ms = 60,000 s
```

- [deep sleep](https://development.pycom.io/firmwareapi/pycom/machine/), modo de economia de energia que [desliga tudo menos 
o RTC](https://lastminuteengineers.com/esp32-sleep-modes-power-consumption/#esp32-deep-sleep), dando reboot após terminar :
```
import machine
machine.deepsleep(60000)   # 60000 ms = 60,000 s
```


### 4.2. Alimentação via bateria, medindo via multímetro
