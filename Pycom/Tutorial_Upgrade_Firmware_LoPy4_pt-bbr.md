# Tutorial de atualização do firmware do Pycom LoPy4 #

##### Autor : Pedro Henrique Robadel da Silva Camâra (ph.robadel arroba gmail.com)
##### Última atualização : 24/06/2020.

### Lista de equipamentos utilizados neste tutorial ### 

* Pycom LoPy4
* Pycom Expansion Board v2.1
* Jumper (macho - macho)
* Cabo USB - MicroUSB
* Pycom Antena LoRa

Como mostra a imagem a seguir:

![Lista de equipamentos](img/listaEquipamentos.jpg)

Nota: Não é necessário utilizar Pycom Antena LoRa para a atualização.

### Download e Instalação ###

É necessário baixar o software que atualizará o firmware do LoPy4. Ele está disponível no [site oficial pycom](https://pycom.io/downloads/). O software está disponível para Windows, MacOS e Linux. Basta baixar a versão compatível com o sistema operacional que você usará para a atualização.

Neste Tutorial será utilizada a versão para linux. As opções para Linux são basicamente duas, para ubuntu e superiores (com extensão ‘.deb’), e para Linux genérico (com extensão ‘.tar.gz’). 

**Utilizando Ubuntu 18.04 (Bionic) amd64**

Baixe o arquivo “pycom-fwtool-1.16.0-bionic-amd64.deb”. É necessário abrir o terminal na pasta onde está o arquivo e digitar:

`$ sudo dpkg -i pycom-fwtool-1.16.0-bionic-amd64.deb`

Após digitar a senha do sudo, iniciará a instalação do pycom-fwtool. Para executar basta digitar `$ pycom-fwtool` no terminal.

**Utilizando linux genérico**

Baixado o arquivo “pycom_firmware_update_1.16.0-amd64.tar.gz” é necessário descompactar o arquivo. Para executar basta abrir no terminal a pasta que será extraída (pyupgrade/), e executar o arquivo ‘pycom-fwtool’ com o seguinte comando:

`$ ./pycom-fwtool`

### Atualização do firmware ###

Antes de iniciar a atualização é necessário conectar o LoPy4 no Expansion Board, e com um jumper macho-macho,  conecte o **pino G23 no GND** . Com o cabo USB, conecte o Expansion Board no Computador. Como na imagem a seguir.

![LoPy4 conectado ao computador](img/LoPyConectado.jpg)

Execute o pycom-fwtool e siga a sequência de print screen tirada durante a atualização do firmware.

![Tela1](img/Tela1.jpg)

Esta é a janela de boas vindas, ela informa a versão mais recente do firmware, que será instalada futuramente. click em ‘Continue’ para prosseguir com a instalação.

![Tela2](img/Tela2.jpg)

Esta é a janela de Instruções da configuração. Click em ‘Continue’ para prosseguir com a instalação.

![Tela3](img/Tela3.jpg)

É nesta janela onde se deve selecionar a porta onde o LoPy4 está conectado ao computador,  a velocidade da comunicação e o local onde o firmware está. Com a opção ‘Flash from local file’ desativada, o programa baixará o firmware. Caso já tenha o firmware no computador, deve-se informar o local do arquivo em seu computador. Neste tutorial seguiu com a opção de baixar  firmware. click em ‘Continue’ para prosseguir com a instalação.

![Tela4](img/Tela4.jpg)

Nesta etapa basta esperar que o firmware baixe e seja instalado.

![Tela5](img/Tela5.jpg)

Caso a instalação tenha sido realizada com sucesso, a janela a seguir será exibida informando o resultado da instalação. click em ‘Done’ para finalizar com a instalação.

### Confirmando a atualização  ###

Desconecte o cabo USB e remova o jumper. Conecte o cabo USB novamente e teste conexão USB serial via screen/rshell utilizando:

`$ screen /dev/ttyUSB0 115200`

ou

`$ rshell -p /dev/ttyUSB0 -b 115200 --buffer-size 32`

---

(A FAZER) :
- versão p/ Expansion Board v3.x;
- versão em inglês.