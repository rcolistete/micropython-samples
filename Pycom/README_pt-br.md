# MicroPython em placas Pycom

Tutoriais, exemplos, testes (benchmarks), firmwares, etc, com MicroPython em placas Pycom

A FAZER :
- Usando Expansion Board;
- Atualizando Firmware Pycom MicroPython;
- Usando shells, editores e IDE's com Pycom MicroPython;
- Usando PySense;
- Usando LoPy v1;
- Usando WiPy v2;
- Usando WiPy v1;
- exemplos sobre :
    - GPIO com benchmark de chaveamento de saída;
    - I2C;
    - ADC com benchmark;
    - DAC com benchmark;
    - RTC;
    - NVRAM (nvs_*);
    - atualização de firmware de modem LTE no FiPy;
    - etc;
- testes de consumo de energia com alimentação por bateria;
- testes de tempo para acordar de lightsleep e deepsleep;
- firmwares Pycom personalizados com :
    - módulo nativo ulab;
    - módulos 'frozen';
    - precisão dupla em ponto flutuante;
    - correção de bugs LoRa(Wan);
    - configuração p/ não usar memória PSRAM;
    - etc;
- versões traduzidas para inglês;
